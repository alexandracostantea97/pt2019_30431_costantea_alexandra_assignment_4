package controller;

import BusinessLayer.IRestaurantProcessing;
import BusinessLayer.MenuItem;
import BusinessLayer.Order;
import BusinessLayer.Restaurant;
import DataLayer.Reader;
import DataLayer.Writer;
import PresentationLayer.AdministratorGUI;
import PresentationLayer.WaiterGUI;

import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

public class Controller {
    private AdministratorGUI administratorGUI ;
    private WaiterGUI waiterGUI ;
    private IRestaurantProcessing restaurant ;


   public Controller(){
       restaurant = Reader.read();
       if(restaurant == null){
           restaurant = new Restaurant();
       }
       administratorGUI = new AdministratorGUI();
       administratorGUI.refresh((Restaurant) restaurant);
       waiterGUI = new WaiterGUI();
       waiterGUI.refresh((Restaurant) restaurant);
       addActWaiter();
       AddActAdmin();
   }

   public static void main(String[] args){
       Controller controller = new Controller();
   }

    public void addActWaiter(){
       waiterGUI.addWindowListener(new WindowAdapter() {
           @Override
           public void windowClosing(WindowEvent e) {
                Writer.write((Restaurant) restaurant);

           }
       });

        waiterGUI.getInsertB().addActionListener((ActionEvent e) -> {
            int tableId = Integer.parseInt(waiterGUI.getInsertTF().getText());
            Order oder = restaurant.createNewOrder(tableId);
            waiterGUI.addOrder(oder);
        });

        waiterGUI.getCreateBIllB().addActionListener((ActionEvent e) -> {
            int orderId = Integer.parseInt(waiterGUI.getTable().getValueAt(waiterGUI.getTable().getSelectedRow(),0).toString());
            restaurant.genetareBill(orderId);
        });
    }

    public void AddActAdmin(){

        administratorGUI.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                Writer.write((Restaurant) restaurant);

            }
        });

       administratorGUI.getAddItemB().addActionListener((ActionEvent e) -> {
           String productName = administratorGUI.getProductNameTf().getText();
           int productPrice = Integer.parseInt(administratorGUI.getProductPriceTf().getText());
           MenuItem item  = restaurant.createMenuItem(productName,productPrice);
           administratorGUI.addMenuItem(item);
       });
       administratorGUI.getAddToOrderB().addActionListener((ActionEvent e) -> {
           int productId = Integer.parseInt(administratorGUI.getTable().getValueAt(administratorGUI.getTable().getSelectedRow(),0).toString());
           int orderId = Integer.parseInt(waiterGUI.getTable().getValueAt(waiterGUI.getTable().getSelectedRow(),0).toString());
            restaurant.addMenuItem(orderId,productId);

       });

       administratorGUI.getDeleteItemB().addActionListener((ActionEvent e) -> {
            int productId = Integer.parseInt(administratorGUI.getTable().getValueAt(administratorGUI.getTable().getSelectedRow(),0).toString());
            restaurant.deleteMenuItem(productId);
            administratorGUI.refresh((Restaurant) restaurant);

       });

       administratorGUI.getUpdateItemB().addActionListener((ActionEvent e) -> {
            int productId = Integer.parseInt(administratorGUI.getTable().getValueAt(administratorGUI.getTable().getSelectedRow(),0).toString());
            String productName = administratorGUI.getProductNameTf().getText();
            int productPrice = Integer.parseInt(administratorGUI.getProductPriceTf().getText());

            restaurant.editMenuItem(productId,productName,productPrice);
            administratorGUI.refresh((Restaurant) restaurant);
           administratorGUI.refresh((Restaurant) restaurant);

       });
    }
}
