package DataLayer;

import BusinessLayer.IRestaurantProcessing;
import BusinessLayer.Restaurant;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Date;

public class Writer {

    public static void write(Restaurant restaurant) {
        try {
            FileOutputStream fos = new FileOutputStream("test.ser");
            ObjectOutputStream out = new ObjectOutputStream(fos);
            out.writeObject(restaurant);
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
