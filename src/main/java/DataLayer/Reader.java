package DataLayer;

import BusinessLayer.IRestaurantProcessing;
import BusinessLayer.Restaurant;

import java.io.*;

public class Reader {

    public static Restaurant read() {
        try{
            FileInputStream fis = new FileInputStream("test.ser");
            ObjectInputStream in = new ObjectInputStream(fis);
            Restaurant restaurant = (Restaurant) in.readObject();
            fis.close();
            return restaurant;
        }
        catch(FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        catch(IOException e){
            e.printStackTrace();
            return null;
        }
        catch (ClassNotFoundException e){
            e.printStackTrace();
            return null;
        }
    }
}
