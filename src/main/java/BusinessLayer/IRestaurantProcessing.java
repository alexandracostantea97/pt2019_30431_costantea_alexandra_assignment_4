package BusinessLayer;

public interface IRestaurantProcessing {

         Order createNewOrder(int table);
         int computePrice(Order order);
         void genetareBill(int orderId);
         void addMenuItem(int orderId, int id);
         MenuItem createMenuItem(String productName, int price);
         void deleteMenuItem(int id);
         void editMenuItem(int id, String productName, int price);

}
