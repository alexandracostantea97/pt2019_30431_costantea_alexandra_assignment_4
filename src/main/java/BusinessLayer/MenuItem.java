package BusinessLayer;

public interface MenuItem {
    int computePrice();
    int getId();
    String getProductName();

    void setProductName(String productName);

    void setProductPrice(int price);
}
