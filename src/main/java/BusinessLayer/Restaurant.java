package BusinessLayer;


import lombok.Getter;
import lombok.Setter;

import java.awt.*;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.lang.management.MemoryNotificationInfo;
import java.util.*;
import java.util.List;

@Getter
@Setter
public class Restaurant extends Observable implements IRestaurantProcessing, Serializable {
    private ArrayList<Observer> observers;
    private HashMap<Order, ArrayList<MenuItem>> mappedOrders = new HashMap<Order,ArrayList<MenuItem>>();
    private int currentOrderId = 1;
    private List<MenuItem> items = new ArrayList<>();
    private int currentItemId = 1;
    public Restaurant() {
        this.observers = new ArrayList<>();
    }

    @Override
    public synchronized void addObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public synchronized void deleteObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers() {
        System.out.println("Notifying all observers when a change is made.");
        for (Observer o : observers) {
            o.update(this, this);
        }
    }

    public void addOrder(Order o){
        mappedOrders.put(o,new ArrayList<>());
    }

    public void removeOrder(Order o){
        if(mappedOrders.containsKey(o)){
            mappedOrders.remove(o);
        }
    }

    public void addMenuItem(Order o, MenuItem menuItem){
        if(mappedOrders.containsKey(o)){
            mappedOrders.get(o).add(menuItem);
        }
        else
        {
            System.out.println("Cannot insert menuItem, Order does not exist");
        }
    }

    private MenuItem getItem(int id){
        MenuItem item = null;
        for(MenuItem i: items){
            if(i.getId() == id){
                item = i;
            }
        }
         return item;

    }

    @Override
    public void editMenuItem(int id, String productName, int price) {
        MenuItem item = getItem(id);
        if(item == null){
            System.out.println("Cannot edit, item not found.");
            return;
        } else {
            item.setProductName(productName);
            item.setProductPrice(price);
        }
    }

    @Override
    public void deleteMenuItem(int id) {
        MenuItem item = getItem(id);
        if(items.contains(item)){
            items.remove(item);
        } else {
            System.out.println("Cannot remove item. Item not found.");
        }
    }

    public void addMenuItem(int orderId, int id){
       MenuItem item = null;
       for(MenuItem i: items){
           if(i.getId()== id){
               item = i;
           }
       }
       Order o = null;
       for(Order order: mappedOrders.keySet()){
           if(order.getOrderId() == orderId){
               o = order;
           }
       }
       if(item == null || o == null){
           System.out.println("item not found");
       }
       else{
           addMenuItem(o,item);
       }
    }

    @Override
    public MenuItem createMenuItem(String productName, int price) {
        MenuItem item = new BaseProduct(currentItemId++,productName, price);
        items.add(item);
        return item;
    }

    @Override
    public Order createNewOrder(int table) {
        Order newOrder = new Order(currentOrderId++,new Date(),table);
        addOrder(newOrder);
        return newOrder;
    }

    @Override
    public int computePrice(Order order) {
        int sum = 0;
        for(MenuItem item: mappedOrders.get(order)){
            sum += item.computePrice();
        }
        return sum;
    }

    @Override
    public void genetareBill(int orderId) {
        Order order = null;
        for(Order o: mappedOrders.keySet()){
            if(o.getOrderId() == orderId){
                order = o;
            }
        }

        if(order == null){
            System.out.println("Cannot generate bill, order does not exist");
            return;
        }
        try{
            PrintWriter writer = new PrintWriter("bill_" + order.getOrderId() + ".txt", "UTF-8");
            writer.println("Order: " + order.getOrderId());
            writer.println("Items:");
            for(MenuItem item : mappedOrders.get(order)){
                writer.println(item.getProductName() + ": " + item.computePrice() + " lei,");
            }
            writer.println("Total: " + computePrice(order));
            writer.println("\n" + new Date());
            writer.close();
        }
        catch(FileNotFoundException e){
            e.printStackTrace();
        }
        catch(UnsupportedEncodingException e){
            e.printStackTrace();
        }
    }
}
