package BusinessLayer;


import java.util.ArrayList;
import java.util.List;

public class CompositeProduct implements MenuItem {
    private List<MenuItem> products = new ArrayList<>();
    private int productId;
    private String productName;

    public CompositeProduct(int productId, String productName) {
        this.productId = productId;
        this.productName = productName;
    }

    @Override
    public int getId() {
        return productId;
    }

    @Override
    public String getProductName() {
        return productName;
    }

    @Override
    public void setProductPrice(int price) {
        System.out.println("Cannot set price of composite item");
    }

    @Override
    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Override
    public int computePrice() {
        int sum = 0;
        for (MenuItem mI : products) {
            sum += mI.computePrice();
        }
        return sum;
    }

    public void addProduct(MenuItem menuItem) {
        products.add(menuItem);
    }

    public void removeProduct(MenuItem menuItem) {
        products.remove(menuItem);
    }

    public List<MenuItem> getProducts() {
        return products;
    }

    public void setProducts(List<MenuItem> products) {
        this.products = products;
    }

}
