package BusinessLayer;

import java.io.Serializable;
import java.util.Date;

public class Order implements Serializable {
    private int orderId;
    private Date date;
    private int table;

    public Order(int orderId, Date date, int table) {
        this.orderId = orderId;
        this.date = date;
        this.table = table;
    }


    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getTable() {
        return table;
    }

    public void setTable(int table) {
        this.table = table;
    }

    public int hashCode(){
        return  orderId * table + date.hashCode();
    }
}
