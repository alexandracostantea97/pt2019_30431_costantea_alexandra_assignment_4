package PresentationLayer;

import BusinessLayer.Order;
import BusinessLayer.Restaurant;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.Date;

public class WaiterGUI extends JFrame {
    private JTable table;
    private DefaultTableModel model;
    private JPanel panel = new JPanel();
    private JPanel panel1 = new JPanel();
    private JPanel panel2 = new JPanel();
    private JTextField insertTF = new JTextField("table");
    private JButton insertB = new JButton("insertOrder");
    private JButton createBIllB = new JButton("create bill");

    public WaiterGUI() {
        super("Waiter");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(new GridLayout(2,1));
        model = new DefaultTableModel();

        this.table = new JTable(model);
        JScrollPane scrollPane = new JScrollPane(table);
        model.addColumn("id");
        model.addColumn("Date");
        model.addColumn("Table");
        add(scrollPane, BorderLayout.CENTER);

        insertB.setSize(100,30);
        setSize(1000,500);
        panel.setLayout(new GridLayout(2,1));
        panel1.add(insertTF);
        panel2.add(insertB);
        panel2.add(createBIllB);
        panel.add(panel1);
        panel.add(panel2);
        add(panel);
        setVisible(true);



    }

    public void addRow(int orderID, Date orderDate, int table){
        //DefaultTableModel model = (DefaultTableModel) (this.table.getModel());
        model.addRow(new Object[] {orderID, orderDate, table});
    }

    public void addOrder(Order order){
        this.addRow(order.getOrderId(),order.getDate(),order.getTable());
    }


    public static void main(String args[]){
        WaiterGUI waiter = new WaiterGUI();
        waiter.addRow(5,new Date(), 4);
        waiter.addOrder(new Order(6,new Date(),9));
    }

    public JTextField getInsertTF() {
        return insertTF;
    }

    public JTable getTable() {
        return table;
    }

    public JButton getInsertB() {
        return insertB;
    }

    public JButton getCreateBIllB() {
        return createBIllB;
    }

    public void refresh(Restaurant restaurant){
       model.setRowCount(0);
        for(Order order: restaurant.getMappedOrders().keySet()){
            addOrder(order);
        }
    }
}
