package PresentationLayer;

import BusinessLayer.BaseProduct;
import BusinessLayer.MenuItem;
import BusinessLayer.Order;
import BusinessLayer.Restaurant;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class AdministratorGUI extends JFrame{
    private JTable table;
    private DefaultTableModel model;
    private JPanel panel = new JPanel();
    private JPanel panel1 =  new JPanel();
    private JPanel panel2 = new JPanel();
    private JTextField productNameTf = new JTextField("Product name");
    private JTextField productPriceTf = new JTextField("Product price");
    private JButton addToOrderB = new JButton("addItem");
    private JButton addItemB = new JButton("createItem");
    private JButton deleteItemB = new JButton("deleteItem");
    private JButton updateItemB = new JButton("updateItem");

    public AdministratorGUI() {
        super("Administrator");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(new GridLayout(2,1));
        model = new DefaultTableModel();

        this.table = new JTable(model);
        JScrollPane scrollPane = new JScrollPane(table);
        model.addColumn("id");
        model.addColumn("Product name");
        model.addColumn("Price");
        add(scrollPane, BorderLayout.CENTER);

        panel.setLayout(new GridLayout(2,1));
        panel1.add(productNameTf);
        panel1.add(productPriceTf);
        panel2.add(addToOrderB);
        panel2.add(addItemB);
        panel2.add(deleteItemB);
        panel2.add(updateItemB);
        panel.add(panel1);
        panel.add(panel2);
        add(panel);
        setSize(500,500);
        setVisible(true);



    }

    public void addRow(int productId, String productName, int price){
        model.addRow(new Object[] {productId, productName, price});
    }

    public void addMenuItem(MenuItem menuItem){
        this.addRow(menuItem.getId(), menuItem.getProductName(), menuItem.computePrice());
    }


    public JTextField getProductNameTf() {
        return productNameTf;
    }

    public JTextField getProductPriceTf() {
        return productPriceTf;
    }

    public JButton getAddToOrderB() {
        return addToOrderB;
    }

    public JButton getAddItemB() {
        return addItemB;
    }

    public JButton getDeleteItemB() {
        return deleteItemB;
    }

    public JButton getUpdateItemB() {
        return updateItemB;
    }

    public JTable getTable() {
        return table;
    }

    public void refresh(Restaurant restaurant){
        model.setRowCount(0);
        List<MenuItem> items;

        items = restaurant.getItems();
        for(MenuItem item: items){
            addMenuItem(item);
        }
    }

    public static void main(String args[]){
        AdministratorGUI admin = new AdministratorGUI();
        admin.addRow(5,"mici", 4);
        admin.addMenuItem(new BaseProduct(3,"snitel",15));
    }
}
